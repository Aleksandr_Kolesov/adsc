package com.epam.linkedlist;

public class MyLinkedList<T>
{

    public Node getFirst()
    {
        return first;
    }

    public Node getLast()
    {
        return last;
    }

    public int getSize()
    {
        return size;
    }

    protected Node first = null;
    private Node last = null;
    protected Integer size = 0;

    public void addLast(T data)
    {
        if (last == null)
        {
            first = last = new Node(data);
        }
        else
        {
            last.next = new Node(data);
            last.next.prev = last;
            last = last.next;
        }
        size++;
    }

    public void addFirst(T data)
    {
        if (first == null)
        {
            first = last = new Node(data);
        }
        else
        {
            Node tmp = first;
            first = new Node(data);
            tmp.prev = first;
            first.next = tmp;
        }
        size++;

    }

    public void delete(T data)
    {
        if (first.data.equals(data))
        {
            first = first.next;
            if (first == null)
            {
                last = null;
            }
            else
            {
                first.prev = null;
            }
            size--;
        }
        else
        {
            Node point = first.next;
            while (point != null)
            {
                if (point.data.equals(data))
                {
                    point.prev.next = point.next;
                    if (point.next == null)
                    {
                        last = point.prev;
                    }
                    size--;

                }
                point = point.next;
            }
        }
    }

    @Override
    public String toString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        Node pointer = first;
        while (pointer != null)
        {
            stringBuilder.append(pointer.data);
            if (pointer.next != null)
            {
                stringBuilder.append("->");
            }
            pointer = pointer.next;
        }
        return stringBuilder.toString();
    }

    //2.1 120 minute spent
    static public void dropDuplicates(MyLinkedList<Integer> list)
    {
        MyLinkedList<Integer>.Node pointer1 = list.first;

        while (pointer1 != null)
        {
            MyLinkedList<Integer>.Node pointer2 = pointer1.next;
            while (pointer2 != null)
            {
                if (pointer1.data.equals(pointer2.data))
                {
                    pointer2.prev.next = pointer2.next;
                    if (pointer2.next != null)
                    {
                        pointer2.next.prev = pointer2.prev;
                    }
                    else
                    {
                        list.last = pointer2.prev;
                    }
                    list.size--;
                }
                pointer2 = pointer2.next;

            }
            pointer1 = pointer1.next;
        }
    }

    //2.4 45 minute spent(problem with understanding task)
    static public MyLinkedList<Integer> splitList(MyLinkedList<Integer> list, Integer splitter)
    {
        MyLinkedList newList = new MyLinkedList();
        MyLinkedList<Integer>.Node pointer = list.first;
        while (pointer != null)
        {
            if (pointer.data < splitter)
            {
                newList.addFirst(pointer.data);
            }
            else
            {
                newList.addLast(pointer.data);
            }
            pointer = pointer.next;
        }
        return newList;

    }

    //2.5    60 minute spent
    static public MyLinkedList<Integer> sum(MyLinkedList<Integer> x, MyLinkedList<Integer> y, boolean isReverseOrder)
    {
        StringBuilder stringBuilderX = new StringBuilder();
        StringBuilder stringBuilderY = new StringBuilder();
        MyLinkedList<Integer>.Node pointer = null;
        if (isReverseOrder)
        {
            pointer = x.last;
            while (pointer != null)
            {
                stringBuilderX.append(pointer.data);
                pointer = pointer.prev;
            }
            pointer = y.last;
            while (pointer != null)
            {
                stringBuilderY.append(pointer.data);
                pointer = pointer.prev;
            }

        }
        else
        {
            pointer = x.first;
            while (pointer != null)
            {
                stringBuilderX.append(pointer.data);
                pointer = pointer.next;
            }
            pointer = y.first;
            while (pointer != null)
            {
                stringBuilderY.append(pointer.data);
                pointer = pointer.next;
            }
        }
        Integer sum = Integer.valueOf(stringBuilderX.toString()) + Integer.valueOf(stringBuilderY.toString());
        MyLinkedList<Integer> result = new MyLinkedList<>();
        Integer remainder = sum;
        while (remainder != 0)
        {
            if (isReverseOrder)
            {
                result.addLast(remainder % 10);
            }
            else
            {
                result.addFirst(remainder % 10);
            }

            remainder = remainder / 10;

        }
        return result;

    }

    //2.6 30 minutes spent
    static public boolean isPalindrome(MyLinkedList<String> list)
    {
        MyLinkedList<String>.Node directDirectionPointer = list.first;
        MyLinkedList<String>.Node reverseDirectionPointer = list.last;
        int halfSize = list.size / 2;
        int counter = 0;
        while (counter < halfSize)
        {
            if (!directDirectionPointer.data.equals(reverseDirectionPointer.data))
            {
                return false;
            }
            directDirectionPointer = directDirectionPointer.next;
            reverseDirectionPointer = reverseDirectionPointer.prev;
            counter++;

        }

        return true;

    }

    public class Node
    {

        public Node getNext()
        {
            return next;
        }

        public void setNext(Node next)
        {
            this.next = next;
        }

        public Node getPrev()
        {
            return prev;
        }

        public void setPrev(Node prev)
        {
            this.prev = prev;
        }

        public T getData()
        {
            return data;
        }

        public void setData(T data)
        {
            this.data = data;
        }

        private Node next;
        private Node prev;

        private T data;

        public Node(T data)
        {
            this.data = data;
        }

    }

}
