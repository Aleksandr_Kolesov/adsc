package com.epam.hardtask;

public class HistogramWaterVolume
{
    //    17.21 2 hours spent
    public static int calculateHistogramWaterVolume(int[] histogram)
    {
        int begin = 0;
        int finish = histogram.length - 1;
        int maxHeightIdx = findMaxHeight(histogram, begin, finish);
        int leftVolume = subHistoVolumeLeft(histogram, begin, maxHeightIdx);
        int rightVolume = subHistoVolumeRight(histogram, maxHeightIdx, finish);
        return leftVolume + rightVolume;
    }

    private static int subHistoVolumeLeft(int[] histogram, int begin, int finish)
    {
        int volume = 0;
        if (begin >= finish)
        {
            return volume;
        }
        int maxHeight = findMaxHeight(histogram, begin, finish - 1);
        volume += borderedVolume(histogram, maxHeight, finish);
        volume += subHistoVolumeLeft(histogram, begin, maxHeight);
        return volume;
    }

    private static int subHistoVolumeRight(int[] histogram, int begin, int finish)
    {
        int volume = 0;
        if (begin >= finish)
        {
            return volume;
        }
        int maxHeight = findMaxHeight(histogram, begin + 1, finish);
        volume += borderedVolume(histogram, begin, maxHeight);
        volume += subHistoVolumeRight(histogram, maxHeight, finish);
        return volume;
    }

    private static int findMaxHeight(int[] histogram, int begin, int finish)
    {
        int maxHeightIndex = begin;
        for (int i = begin + 1; i <= finish; i++)
        {
            if (histogram[maxHeightIndex] < histogram[i])
            {
                maxHeightIndex = i;
            }
        }
        return maxHeightIndex;
    }

    private static int borderedVolume(int[] histogram, int begin, int finish)
    {
        int volume = 0;
        if (begin >= finish)
        {
            return volume;
        }
        int minHeight = Math.min(histogram[begin], histogram[finish]);
        for (int i = begin + 1; i < finish; i++)
        {
            volume += minHeight - histogram[i];
        }
        return volume;
    }
}
