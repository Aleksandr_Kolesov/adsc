package com.epam.hardtask;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//17.14 20 minutes spent
public class MinimalsNumbersFinder
{
    public static List<Integer> findMinimalNumbers(List<Integer> list, int n)
    {
        List<Integer> listForSorting = new ArrayList<>(list);
        Collections.sort(listForSorting);
        return listForSorting.subList(0, n);
    }
}
