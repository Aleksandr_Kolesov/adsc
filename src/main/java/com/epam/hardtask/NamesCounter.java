package com.epam.hardtask;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// 17.7 180 minutes spent
public class NamesCounter
{
    public static Map<String, Integer> mergeSynonymousNames(Map<String, Integer> namesCounter, Map<String, List<String>> synonyms)
    {
        Map<String, String> synonymsToOneValue = new HashMap<>();
        Map<String, Integer> result = new HashMap<>();
//        convert map - choose first synonym and use it for all other matched synonyms
        for (Map.Entry<String, List<String>> entry : synonyms.entrySet())
        {
            for (String newValue : entry.getValue())
            {
                findSynonyms(entry.getKey(), newValue, synonymsToOneValue, synonyms);
            }
        }
        for (Map.Entry<String, Integer> entry : namesCounter.entrySet())
        {
            String name = synonymsToOneValue.get(entry.getKey());
            if (name == null)
            {
                name = entry.getKey();
            }
            Integer counter = result.get(name);
            if (counter != null)
            {
                result.put(name, counter + entry.getValue());

            }
            else
            {
                result.put(name, entry.getValue());
            }
        }
        return result;
    }

    private static void findSynonyms(String key, String value, Map<String, String> map,
        Map<String, List<String>> synonyms)
    {
        if (map.containsKey(value))
        {
            return;
        }
        if (!map.containsKey(key))
        {
            map.put(key, key);
        }
        map.put(value, key);
        List<String> synonymsList = synonyms.get(value);
        if (synonymsList != null)
        {
            for (String newValue : synonymsList)
            {
                findSynonyms(key, newValue, map, synonyms);
            }
        }
    }

}
