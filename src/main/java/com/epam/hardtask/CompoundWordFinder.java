package com.epam.hardtask;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//17.15 1 hour spent
public class CompoundWordFinder
{
    public static String findCompoundWord(List<String> words)
    {

        Collections.sort(words, (o1, o2) -> o2.length() - o1.length());
        Set<String> wordsSet = new HashSet<>(words);
        String maxLengthWord = "";

        for (String string : words)
        {
            if (isCompoundWord(string, wordsSet) && maxLengthWord.isEmpty())
            {
                maxLengthWord = string;
            }

        }
        return maxLengthWord;
    }

    private static boolean isCompoundWord(String string, Set<String> words)
    {
        for (int i = 1; i < string.length(); i++)
        {
            String leftWord = string.substring(0, i);
            String rightWord = string.substring(i);
            if (isCompoundWord(leftWord, words) && isCompoundWord(rightWord, words))
            {
                return true;
            }
            else if (words.contains(leftWord) && words.contains(rightWord))
            {
                return true;
            }

        }
        return false;
    }

}
