package com.epam.hardtask;

//17.5 1 hour spent
public class MaxSubArrayFinder
{
    public static char[] find(char[] array)
    {
        for (int l = array.length; l > 1; l--)
        {
            for (int i = 0; i <= array.length - l; i++)
            {
                if (isSameCountDigitAndLetter(array, i, i + l - 1))
                {
                    return getSubArray(array, i, i + l - 1);
                }
            }
        }
        return null;
    }

    private static boolean isSameCountDigitAndLetter(char[] array, int begin, int finish)
    {
        int letterCounter = 0;
        int digitCounter = 0;
        for (int i = begin; i <= finish; i++)
        {
            if (Character.isLetter(array[i]))
            {
                letterCounter++;
            }
            else if (Character.isDigit(array[i]))
            {
                letterCounter--;
            }
        }
        return letterCounter == digitCounter;
    }

    private static char[] getSubArray(char[] array, int begin, int finish)
    {
        int newArrayLength = finish - begin + 1;
        char[] newArray = new char[newArrayLength];
        System.arraycopy(array, begin, newArray, 0, newArrayLength);
        return newArray;
    }
}
