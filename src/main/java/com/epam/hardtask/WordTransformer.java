package com.epam.hardtask;

import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WordTransformer
{
    public static Deque<String> transformWord(String start, String stop, String[] words)
    {
        Map<String, List<String>> linkedTemplatesMap = generateLinkTemplatesMap(words);
        Set<String> visited = new HashSet<>();
        return transit(visited, start, stop, linkedTemplatesMap);
    }

    private static Deque<String> transit(Set<String> visited, String start, String stop,
        Map<String, List<String>> wildcardToWordList)
    {
        if (start.equals(stop))
        {
            Deque<String> path = new LinkedList<>();
            path.add(start);
            return path;
        }
        else if (visited.contains(start))
        {
            return null;
        }
        visited.add(start);
        List<String> words = getOneChangesWords(start, wildcardToWordList);

        for (String word : words)
        {
            Deque<String> path = transit(visited, word, stop, wildcardToWordList);
            if (path != null)
            {
                path.addFirst(start);
                return path;
            }
        }
        return null;
    }

    private static Map<String, List<String>> generateLinkTemplatesMap(String[] words)
    {
        Map<String, List<String>> linkTemplates = new HashMap<>();
        for (String word : words)
        {
            List<String> templates = generateTemplates(word);
            for (String template : templates)
            {
                List<String> list = linkTemplates.computeIfAbsent(template, k -> new ArrayList<>());
                list.add(word);
            }
        }
        return linkTemplates;
    }

    private static List<String> generateTemplates(String word)
    {
        List<String> templates = new ArrayList<>();
        for (int i = 0; i < word.length(); i++)
        {
            char[] charArray = word.toCharArray();
            charArray[i] = '_';
            templates.add(new String(charArray));
        }
        return templates;
    }

   private static List<String> getOneChangesWords(String word, Map<String, List<String>> wildcardToWords)
    {
        List<String> templates = generateTemplates(word);
        List<String> oneChangesWord = new ArrayList<>();
        for (String wildcard : templates)
        {
            List<String> list = wildcardToWords.get(wildcard);
            for (String linkWord : list)
            {
                if (!linkWord.equals(word))
                {
                    oneChangesWord.add(linkWord);
                }
            }
        }
        return oneChangesWord;
    }
}
