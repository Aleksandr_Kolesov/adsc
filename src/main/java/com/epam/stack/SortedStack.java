package com.epam.stack;
//
public class SortedStack<T extends Comparable<T>> extends MinStack<T>
{
    @Override
    public void push(T data)
    {
        if (first == null || data.compareTo(first.getData()) < 0)
        {
            super.push(data);
        }
        else
        {
            Node pointer = first;
            while (pointer.getNext() != null && data.compareTo(pointer.getNext().getData()) >= 0)
            {
                pointer = pointer.getNext();
            }
            Node newNode = new Node(data);
            newNode.setNext(pointer.getNext());
            pointer.setNext(newNode);
            newNode.setPrev(pointer);
        }
    }
}
