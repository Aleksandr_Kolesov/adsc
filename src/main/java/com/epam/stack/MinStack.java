package com.epam.stack;

import com.epam.linkedlist.MyLinkedList;

import java.util.EmptyStackException;

//3.2 30 minute spent
public class MinStack<T extends Comparable<T>> extends MyLinkedList<T>
{
    private Node min = null;

    public Node pop()
    {
        if (isEmpty())
        {
            throw new EmptyStackException();
        }

        Node tmp = first;
        first = first.getNext();
        if (first != null)
        {
            first.setPrev(null);
        }
        size--;
        return tmp;
    }

    public void push(T data)
    {

        addFirst(data);
        if (min == null || first.getData().compareTo(min.getData()) < 0)
        {
            min = first;
        }
    }

    public boolean isEmpty()
    {
        return getSize() == 0;
    }

    public Node min()
    {
        return min;
    }

}
