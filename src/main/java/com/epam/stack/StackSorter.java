package com.epam.stack;
// 3.5 1 hour spent, see SortedStack
public class StackSorter
{
    public static MinStack<Integer> sort(MinStack<Integer> stack)
    {
        SortedStack<Integer> sortedStack = new SortedStack<>();
        while (!stack.isEmpty())
        {
            sortedStack.push(stack.pop().getData());
        }
        return sortedStack;
    }
}
