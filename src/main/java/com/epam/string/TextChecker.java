package com.epam.string;

import java.util.HashMap;
import java.util.Map;

public class TextChecker
{
    //1.1 10 minutes spent
    public boolean isTextWithUniqSymbols(String text)
    {
        char[] chars = new char[65536];
        for (char symbol : text.toCharArray())
        {
            if (++chars[symbol] > 1)
                return false;

        }
        return true;
    }

    //1.2 30 minute spent
    public boolean isReshuffle(String s1, String s2)
    {
        boolean[] charAlreadyWas = new boolean[65536];
        if (s1.length() != s2.length())
        {
            return false;
        }
        for (char symbol : s1.toCharArray())
        {
            charAlreadyWas[symbol] = true;
        }
        for (char symbol : s1.toCharArray())
        {
            if (!charAlreadyWas[symbol])
            {
                return false;
            }
        }

        return true;
    }

    //1.4 30 minute spent
    public boolean isPalindromePermution(String text)
    {
        text = text.toLowerCase().replaceAll("\\s+", "");
        Map<Character, Integer> symbolsCounter = new HashMap<>();
        for (char symbol : text.toCharArray())
        {
            Integer counter = symbolsCounter.get(symbol);
            if (counter != null)
            {
                symbolsCounter.put(symbol, ++counter);
            }
            else
            {
                symbolsCounter.put(symbol, 1);
            }

        }
        Integer oddCounter = 0;
        for (Map.Entry<Character, Integer> entry : symbolsCounter.entrySet())
        {
            if (entry.getValue() % 2 == 1)
            {
                if (++oddCounter > 1)
                {
                    return false;
                }

            }
        }
        return true;
    }

    //1.5 60 minutes spent
    public boolean isDistanceOfOneModification(String line1, String line2)
    {

        if (!line1.equals(line2))
        {
            char[] charArrayLine1 = line1.toCharArray();
            char[] charArrayLine2 = line2.toCharArray();
            int i1 = 0;
            int i2 = 0;
            if (line1.length() == line2.length())
            {
                int mismatchCounter = 0;

                for (int i = 0; i < charArrayLine1.length; i++)
                {
                    if (charArrayLine1[i] != charArrayLine2[i])
                    {
                        if (++mismatchCounter > 1)
                        {
                            return false;
                        }
                    }

                }
            }
            else if (line1.length() > line2.length())
            {

                while (i1 < line1.length() && i2 < line2.length())
                {
                    if (charArrayLine1[i1] != charArrayLine2[i2])
                    {
                        if (charArrayLine1[++i1] != charArrayLine2[i2])
                        {
                            return false;
                        }
                    }
                    i1++;
                    i2++;
                }

            }
            else
            {

                while (i1 < line1.length() && i2 < line2.length())
                {
                    if (charArrayLine1[i1] != charArrayLine2[i2])
                    {
                        if (charArrayLine1[i1] != charArrayLine2[++i2])
                        {
                            return false;
                        }
                    }
                    i1++;
                    i2++;
                }
            }
        }

        return true;
    }

    //1.6 30 minutes spent
    String compressString(String text)
    {
        StringBuilder stringBuilder = new StringBuilder();
        char[] charArray = text.toCharArray();
        char previousCharacter = charArray[0];
        checkSymbol(previousCharacter);
        boolean isCompressed = false;
        int charCounter = 1;

        for (int i = 1; i < text.length(); i++)
        {
            char character = charArray[i];
            checkSymbol(character);
            if (character == previousCharacter)
            {
                charCounter++;
                isCompressed = true;
            }
            else
            {
                appendCompressedChar(stringBuilder, previousCharacter, charCounter);
                previousCharacter = character;
                charCounter = 1;
            }
        }
        appendCompressedChar(stringBuilder, previousCharacter, charCounter);
        if (isCompressed)
        {
            return stringBuilder.toString();
        }
        else
        {
            return text;
        }

    }

    private void appendCompressedChar(StringBuilder stringBuilder, char previousCharacter, int charCounter)
    {
        stringBuilder.append(previousCharacter);
        stringBuilder.append(charCounter);
    }

    private void checkSymbol(char character)
    {
        if ((character < 'A' || character > 'Z') && (character < 'a' || character > 'z'))
        {
            throw new IllegalArgumentException("text contains illegal symbol");
        }

    }

}
