package com.epam.sorter;

import java.util.Collections;
import java.util.List;

public class PeaksSorter
{
//    10.11 1 hour spent
    public static void sortPeaks(List<Integer> list)
    {
        Collections.sort(list);
        for (int i = 1; i < list.size(); i += 2)
        {
            Collections.swap(list, i + 1, i);
        }
    }

}
