package com.epam.sorter;

import java.util.Arrays;
import java.util.List;

//10.3 spent 1 hour
public class ShiftedFounder
{

    public int find(List<Integer> list, int f)
    {
        int low = 0;
        int high = list.size() - 1;
        int mid;
        while (low < high - 1)
        {
            mid = (low + high) / 2;
            if (list.get(mid) > list.get(list.size() - 1))
            {
                low = mid;
            }
            else
            {
                high = mid;
            }
        }
        int iMax = low;
        if (f >= list.get(0))
        {
            low = 0;
            high = iMax;
        }
        else if (f < list.get(list.size() - 1))
        {
            low = iMax + 1;
            high = list.size() - 1;
        }
        else
            return -1;
        while (low <= high)
        {
            mid = (low + high) / 2;
            if (list.get(mid) < f)
            {
                low = mid + 1;
            }
            else if (list.get(mid) > f)
            {
                high = mid - 1;
            }
            else
                return mid;

        }
        return -1;
    }

    public static void main(String[] args)
    {
        List<Integer> list = Arrays.asList(15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 13, 14);
        int aa = new ShiftedFounder().find(list, 0);
        System.out.println();

    }

}
