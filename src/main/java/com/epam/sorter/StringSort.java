package com.epam.sorter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

//10.2 180 minute spent
public class StringSort
{
    private String cleanAndSortStr(String str1)
    {
        char[] charArray = str1.toLowerCase().replaceAll("\\s+", "").toCharArray();
        Arrays.sort(charArray);
        return new String(charArray);
    }

    public List<String> sort(List<String> strings)
    {
        List<String> stringsForSorting = new ArrayList<>(strings);
        Collections.sort(stringsForSorting, Comparator.comparing(this::cleanAndSortStr));
        return stringsForSorting;

    }

}
