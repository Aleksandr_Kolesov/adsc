package com.epam.testtasks;

import com.epam.graph.Graph;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

//O(sqr(n))
public class GraphWithRemover extends Graph
{
    public GraphWithRemover(int size)
    {
        super(size);
    }

    @Override
    public void addEdge(int from, int to)
    {
        super.addEdge(from, to);
        super.addEdge(to, from);
    }

    public void removeVertexWith2Edge()
    {
        for (int i = 0, nodesLength = nodes.length; i < nodesLength; i++)
        {
            Node node = nodes[i];
            if (node != null)
            {
                Deque<Node> deque = new LinkedList<>();
                node.marked = true;
                deque.push(node);
                while (!deque.isEmpty())
                {
                    Node n = deque.pop();
                    removeIfDeg2(n);
                    for (Node child : n.children)
                    {
                        if (child != null && !child.marked)
                        {
                            child.marked = true;
                            deque.push(child);
                        }
                    }
                }

            }
        }

    }

    private void removeIfDeg2(Node n)
    {
        List<Node> children = new ArrayList<>();
        for (Node child : n.children)
        {
            if (child != null)
            {
                children.add(child);
            }
        }
        if (children.size() == 2)
        {
            int i1 = findIndex(children.get(0), n);
            int i2 = findIndex(children.get(1), n);
            if (findIndex(children.get(0), children.get(1)) == -1)
            {

                children.get(0).children[i1] = children.get(1);
                children.get(1).children[i2] = children.get(0);
            }
            else
            {
                children.get(0).children[i1] = null;
                children.get(1).children[i2] = null;
            }
            children.get(0).marked = false;
            children.get(1).marked = false;
            n.children = new Node[0];
            removeFromNodes(n);
        }

    }

    private void removeFromNodes(Node n)
    {
        for (int i = 0; i < nodes.length; i++)
        {
            if (nodes[i] == n)
            {
                nodes[i] = null;
            }

        }
    }

    private int findIndex(Node node, Node n)
    {
        for (int i = 0; i < node.children.length; i++)
        {
            if (node.children[i] == n)
            {
                return i;
            }
        }
        return -1;
    }

}
