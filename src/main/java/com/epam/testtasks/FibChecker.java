package com.epam.testtasks;

import java.util.ArrayList;
import java.util.List;

// O(sqrt(n))
public class FibChecker
{
    public static boolean check(String seq)
    {
        if (seq.length() < 3)
        {
            return false;
        }

        int divider = 3;

        while (divider <= seq.length())
        {
            int k = 0;
            int s = seq.length();
            List<String> list = new ArrayList<>();
            for (int i = divider; i >= 2; i--)
            {
                int d = s / i;
                list.add(seq.substring(k, k + d));
                k += d;
                s -= d;
            }
            list.add(seq.substring(k, seq.length()));
            if (isFibSeq(list))
            {
                return true;
            }
            divider++;
        }
        return false;

    }

    private static boolean isFibSeq(List<String> list)
    {
        for (int i = 1; i < list.size() - 1; i++)
        {
            Integer a = Integer.valueOf(list.get(i - 1));
            Integer b = Integer.valueOf(list.get(i));
            Integer c = Integer.valueOf(list.get(i + 1));
            if (a + b != c)
            {
                return false;
            }

        }
        return true;
    }


}
