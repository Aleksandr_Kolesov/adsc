package com.epam.graph;

import com.epam.linkedlist.MyLinkedList;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Tree
{
    public Node root;
    private Random random = new Random();
    private int size = 0;

    class Node
    {
        public Integer value;
        public Node left;
        public Node right;
        public boolean visited = false;

        @Override
        public String toString()
        {
            return value.toString();
        }
    }

    public Node addRandomSheet(int value)
    {
        boolean isLeft;
        Node newNode = null;
        if (root == null)
        {
            root = new Node();
            root.value = value;
            newNode = root;
        }
        else
        {
            Node pointer1 = root;
            isLeft = random.nextBoolean();
            Node pointer2 = root;
            if (isLeft)
            {
                pointer2 = root.left;
            }
            else
            {
                pointer2 = root.right;
            }
            while (pointer2 != null)
            {
                pointer1 = pointer2;
                isLeft = random.nextBoolean();
                if (isLeft)
                {
                    pointer2 = pointer2.left;
                }
                else
                {
                    pointer2 = pointer2.right;
                }
            }
            if (isLeft)
            {
                pointer1.left = new Node();
                pointer1.left.value = value;
                newNode = pointer1.left;
            }
            else
            {

                pointer1.right = new Node();
                pointer1.right.value = value;
                newNode = pointer1.right;
            }

        }
        size++;
        return newNode;
    }

    //4.3 2 hours spent (used Prefix traversal)
    public MyLinkedList[] createLinkedLists()
    {
        return fillLinkedLists(root, 0, new MyLinkedList[size]);

    }

    private MyLinkedList[] fillLinkedLists(Node root, int i, MyLinkedList[] array)
    {
        if (root != null)
        {
            MyLinkedList linkedList = array[i];
            if (linkedList == null)
            {
                linkedList = new MyLinkedList();
                array[i] = linkedList;
            }
            linkedList.addLast(root);
            i++;
            fillLinkedLists(root.left, i, array);
            fillLinkedLists(root.right, i, array);
        }
        return array;
    }

    // 4.10 1 hour spent
    public boolean isSubTree(Tree tree)
    {
        Node t1Root = tree.root;
        Node t2Root = this.root;
        List<Node> roots = traversal(t1Root, t2Root, new ArrayList<>());
        for (Node root : roots)
        {
            if (compareTrees(root, t2Root))
            {
                return true;
            }

        }
        return false;

    }


    private boolean compareTrees(Node t1node, Node t2root)
    {

        if (t1node != null && t2root != null)
        {
            if (!t1node.value.equals(t2root.value) && compareTrees(t1node.left, t2root.left) &&
                compareTrees(t1node.right, t2root.right))
            {
                return false;
            }
        }
        return true;
    }

    private List<Node> traversal(Node t1Root, Node t2Root, List<Node> nodes)
    {
        if (t1Root != null)
        {
            if (t1Root.value == t2Root.value)
            {
                nodes.add(t1Root);
            }
            traversal(t1Root.left, t2Root, nodes);
            traversal(t1Root.right, t2Root, nodes);
        }
        return nodes;
    }

}
