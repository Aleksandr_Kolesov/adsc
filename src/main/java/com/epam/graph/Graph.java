package com.epam.graph;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Graph
{
    public Node[] nodes;

    public static class Node
    {
        public String name;
        public State state = State.EMPTY;
        public Node[] children;
        public boolean marked = false;
    }

    public Graph(int size)
    {
        nodes = new Node[size];
        for (int i = 0; i < nodes.length; i++)
        {
            nodes[i] = new Node();
            nodes[i].children = new Node[size];
            nodes[i].name = String.valueOf(i);
        }
    }

    public void addEdge(int from, int to)
    {
        nodes[from].children[to] = nodes[to];
    }

    // 4.1 45 minutes spent(used BFS)
    public boolean haveRoute(int iVertex1, int iVertex2)
    {
        Deque<Node> deque = new LinkedList<>();
        Node vertex1 = nodes[iVertex1];
        deque.offer(vertex1);
        Node vertex2 = nodes[iVertex2];
        while (!deque.isEmpty())
        {
            Node r = deque.poll();
            for (int i = 0; i < r.children.length; i++)
            {
                if (r.children[i] == vertex2)
                {
                    return true;
                }
                if (r.children[i] != null)
                {
                    deque.offer(r.children[i]);
                }

            }
        }
        return false;
    }

    //4.7 2 hours spent
    public List<Node> findOrderProjects()
    {
        List<Node> stack = new LinkedList<>();
        for (Node project : nodes)
        {
            if (project.state == State.EMPTY)
            {
                if (!search(project, stack))
                {
                    return null;
                }
            }
        }
        return stack;
    }

    private boolean search(Node project, List<Node> deque)
    {
        if (project.state == State.ADDED)
        {
            return false;
        }
        if (project.state == State.EMPTY)
        {
            project.state = State.ADDED;
            Node[] children = project.children;
            for (Node child : children)
            {
                if (child != null && !search(child, deque))
                {
                    return false;
                }
            }
            project.state = State.FULL;
            deque.add(project);
        }
        return true;
    }

    public enum State
    {
        EMPTY, ADDED, FULL

    }

}
