package com.epam.mildtask;

//16.9 1 hour spent
public class Math
{
    public static int delete(int x, int y)
    {
        return x + getNegative(y);
    }

    public static int multiply(int x, int y)
    {
        int sum = 0;
        if (y < 0)
        {
            x = getNegative(x);
            y = getNegative(y);
        }
        for (int i = 0; i < y; i++)
        {
            sum += x;
        }
        return sum;
    }

    public static int divide(int x, int y)
    {
        if (y == 0)
        {
            throw new ArithmeticException("denominator == 0");
        }
        int absX = abs(x);
        int absY = abs(y);
        if (absX < absY)
        {
            return 0;
        }
        int sum = absY;
        int result;
        for (result = 0; sum <= absX; result++)
        {
            sum += absY;
        }
        return (x < 0 && y < 0) || (x > 0 && y > 0) ? result : getNegative(result);
    }

    private static int abs(int x)
    {
        if (x < 0)
        {
            x = getNegative(x);
        }
        return x;
    }

    private static int getNegative(int x)
    {
        int negative = 0;
        int oneWithSign;
        oneWithSign = x > 0 ? -1 : 1;
        while (x != 0)
        {
            negative += oneWithSign;
            x += oneWithSign;
        }
        return negative;
    }

}
