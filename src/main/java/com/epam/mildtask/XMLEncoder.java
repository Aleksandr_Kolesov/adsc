package com.epam.mildtask;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//16.12 3 hours spent
public class XMLEncoder

{
    private static Set<String> EXCLUDED_STRINGS = new HashSet<>(Arrays.asList("#text"));

    public static String encodeXML(String path, Map<String, Integer> values)
        throws ParserConfigurationException, IOException, SAXException
    {
        File xmlFile = new File(path);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xmlFile);
        return encodeXML(doc, values);
    }

    private static String encodeXML(Document doc, Map<String, Integer> values)
    {
        StringBuilder stringBuilder = new StringBuilder();
        doc.getDocumentElement().normalize();
        encodeXML(doc.getDocumentElement(), stringBuilder, values);
        return stringBuilder.toString().trim();
    }

    private static void encodeXML(Node documentElement, StringBuilder stringBuilder, Map<String, Integer> values)
    {
        if (values.containsKey(documentElement.getNodeName()))
        {
            appendString(values.get(documentElement.getNodeName()).toString(), stringBuilder);
        }

        NamedNodeMap attributes = documentElement.getAttributes();
        if (attributes != null)
        {
            for (int i = 0; i < attributes.getLength(); i++)
            {
                Node attribute = attributes.item(i);
                appendString(values.get(attribute.getNodeName()).toString(), stringBuilder);
                appendString(attribute.getNodeValue(), stringBuilder);
            }
        }
        if (!isWrongTag(documentElement))
        {
            appendString("0", stringBuilder);
        }
        if (StringUtils.isNotEmpty(documentElement.getNodeValue()))
        {
            appendString(documentElement.getNodeValue(), stringBuilder);
        }
        else
        {
            NodeList children = documentElement.getChildNodes();
            if (children != null)
            {
                for (int i = 0; i < children.getLength(); i++)
                {
                    Node child = children.item(i);
                    encodeXML(child, stringBuilder, values);
                }
            }
        }
        if (!isWrongTag(documentElement))
        {
            appendString("0", stringBuilder);
        }

    }

    private static boolean isWrongTag(Node documentElement)
    {
        return EXCLUDED_STRINGS.contains(documentElement.getNodeName());
    }

    private static void appendString(String tagName, StringBuilder stringBuilder)
    {
        stringBuilder.append(tagName);
        stringBuilder.append(" ");
    }

}
