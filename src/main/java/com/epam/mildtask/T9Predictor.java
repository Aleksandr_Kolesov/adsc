package com.epam.mildtask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//16.20 1 hour spent
public class T9Predictor
{

    static char[][] intTolettets =
        {
            {' '},
            {},
            {'a', 'b', 'c'},
            {'d', 'e', 'f'},
            {'g', 'h', 'i'},
            {'j', 'k', 'l'},
            {'m', 'n', 'o'},
            {'p', 'q', 'r', 's'},
            {'t', 'u', 'v'},
            {'w', 'x', 'y', 'z'}
        };

    public static List<String> predict(String number, List<String> words)
    {
        Map<Character, Integer> lettersToInt = generateMapFromArray(intTolettets);
        Map<String, List<String>> t9codedWord = encodeWords(words, lettersToInt);

        return t9codedWord.get(number);
    }

    private static Map<String, List<String>> encodeWords(List<String> words, Map<Character, Integer> lettersToInt)
    {
        Map<String, List<String>> map = new HashMap<>();
        for (String word : words)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (Character character : word.toCharArray())
            {
                stringBuilder.append(lettersToInt.get(character));
            }
            String t9number = stringBuilder.toString();
            List<String> list = map.computeIfAbsent(t9number, k -> new ArrayList<>());
            list.add(word);
            stringBuilder.setLength(0);
        }
        return map;
    }

    private static Map<Character, Integer> generateMapFromArray(char[][] intTolettets)
    {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < intTolettets.length; i++)
        {
            for (int j = 0; j < intTolettets[i].length; j++)
            {
                map.put(intTolettets[i][j], i);
            }
        }
        return map;
    }
}
