package com.epam.mildtask;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

//16.17 2 hours spent
public class MaxSumSequence
{
    public static Pair<Integer, List<Integer>> findMaxSumSequence(List<Integer> list)
    {
        List<Integer> sequence = new ArrayList<>();
        List<Integer> maxSequence = new ArrayList<>();
        int sumSubSeq = 0;
        int maxSumSubSeq = 0;
        for (Integer aList : list)
        {
            sumSubSeq += aList;
            sequence.add(aList);
            if (sumSubSeq < 0)
            {
                sumSubSeq = 0;
                sequence.clear();
            }
            else if (maxSumSubSeq < sumSubSeq)
            {
                maxSumSubSeq = sumSubSeq;
                maxSequence.clear();
                maxSequence.addAll(sequence);
            }
        }
        return new ImmutablePair<>(maxSumSubSeq, maxSequence);
    }

}
