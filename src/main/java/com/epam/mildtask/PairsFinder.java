package com.epam.mildtask;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//16.24 30 minutes spent
public class PairsFinder
{
    public static List<Pair> findPairs(List<Integer> list, Integer sum)
    {
        List<Pair> result = new ArrayList<>();
        Set<Integer> set = new HashSet<>(list);
        for (Integer a : list)
        {
            int b = sum - a;
            if (set.contains(b))
            {
                result.add(new ImmutablePair(a, b));
                set.remove(a);
                set.remove(b);
            }
        }
        return result;
    }
}
