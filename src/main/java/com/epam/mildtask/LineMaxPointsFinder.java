package com.epam.mildtask;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//16.14 3 hours spent
public class LineMaxPointsFinder
{
    public static Line findMaxPointsInterceptor(Point[] points)
    {
        return findMaxPointsInterceptor(generateLinesBySlopeMap(points));
    }

    private static Map<Double, List<Line>> generateLinesBySlopeMap(Point[] points)
    {
        Map<Double, List<Line>> map = new HashMap<>();
        for (int i = 0; i < points.length; i++)
        {
            for (int j = i + 1; j < points.length; j++)
            {
                Line line = new Line(points[i], points[j]);
                Double slope = Line.roundDouble(line.slope);
                List<Line> list = map.computeIfAbsent(slope, k -> new ArrayList<>());
                list.add(line);
            }
        }
        return map;
    }

    private static Line findMaxPointsInterceptor(Map<Double, List<Line>> map)
    {
        Line maxPointsLine = null;
        int maxCount = 0;

        for (Map.Entry<Double, List<Line>> entry : map.entrySet())
        {
            for (Line line : entry.getValue())
            {
                int count = countEqualsLines(map, line);
                if (maxCount < count)
                {
                    maxPointsLine = line;
                    maxCount = count;
                }
            }
        }
        return maxPointsLine;
    }

    private static int countEqualsLines(Map<Double, List<Line>> linesBySlope, Line line)
    {
        return countEquivalentLines(linesBySlope.get(Line.roundDouble(line.slope)), line);
    }

    private static int countEquivalentLines(List<Line> lines, Line line)
    {
        if (lines == null)
        {
            return 0;
        }

        return (int) lines.stream().filter(l -> l.equalsLine(line)).count();
    }

    public static class Line

    {
        public Double slope;
        public Double interception;
        public static int scale = 5;
        private boolean isAspirationToInfinitySlope = false;

        public Line(Point p1, Point p2)
        {
            if (((Double) p1.x).equals(p2.x))
            {
                isAspirationToInfinitySlope = true;
                interception = p1.x;
            }
            else
            {
                slope = (p1.y - p2.y) / (p1.x - p2.x);
                interception = p1.y - slope * p1.x;
            }
        }

        public static double roundDouble(double dbl)
        {
            return BigDecimal.valueOf(dbl).setScale(scale, BigDecimal.ROUND_HALF_DOWN).doubleValue();
        }

        public boolean equalsLine(Line that)
        {
            return that.slope.equals(this.slope) && that.interception.equals(this.interception) &&
                this.isAspirationToInfinitySlope == that.isAspirationToInfinitySlope;
        }
    }

    public static class Point
    {
        double x, y;

        Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }

}

