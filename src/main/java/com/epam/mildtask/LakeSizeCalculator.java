package com.epam.mildtask;

import java.util.ArrayList;
import java.util.List;

//16.19 1 hour spent
public class LakeSizeCalculator
{
    public static List<Integer> calculateLakesSizes(int[][] land)
    {
        List<Integer> lakesSizes = new ArrayList<>();
        for (int row = 0; row < land.length; row++)
        {
            for (int column = 0; column < land[row].length; column++)
            {
                if (land[row][column] == 0)
                {
                    lakesSizes.add(calculateLakeSize(land, row, column));
                }
            }
        }
        return lakesSizes;

    }

    private static int calculateLakeSize(int[][] land, int row, int column)
    {
        if (
            row < 0 || column < 0
                || row >= land.length || column >= land[row].length
                || land[row][column] != 0
            )
        {
            return 0;
        }
        int size = 1;
        land[row][column] = -1;

        size += calculateLakeSize(land, row + 1, column);
        size += calculateLakeSize(land, row, column + 1);
        size += calculateLakeSize(land, row + 1, column + 1);
        size += calculateLakeSize(land, row - 1, column + 1);
        size += calculateLakeSize(land, row + 1, column - 1);
        size += calculateLakeSize(land, row - 1, column - 1);
        size += calculateLakeSize(land, row, column - 1);
        size += calculateLakeSize(land, row - 1, column);

        return size;
    }

}
