package com.epam.mildtask;

//16.7 1 hour spent
public class MaxFinder
{
    public static int max(int x, int y)
    {

        int signX = getSign(x);
        int signY = getSign(x);
        int signZ = getSign(x - y);

        int hasUsedSignX = signX ^ signY;
        int hasUsedSignY = flip(signX ^ signY);
        int s = hasUsedSignX * signX + hasUsedSignY * signZ;
        int inv = flip(s);
        return x * s + y * inv;

    }

    private static int flip(int b)
    {
        return 1 ^ b;
    }

    private static int getSign(int x)
    {
        return flip((x >> 31) & 0x1);
    }

}
