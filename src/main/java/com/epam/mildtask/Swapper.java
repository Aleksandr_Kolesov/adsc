package com.epam.mildtask;

import java.util.List;

// 16.1 30 minutes
public class Swapper
{
    //    swap elements in array for example 4 and 9 (0100 and 1001)
    public static void swap(List<Integer> list, int a, int b)
    {
        list.set(a, list.get(a) ^ list.get(b)); // 0100^1001 = 1101
        list.set(b, list.get(b) ^ list.get(a)); // 1001^1101 = 0100
        list.set(a, list.get(a) ^ list.get(b)); // 1101^0100 = 1001
    }

}
