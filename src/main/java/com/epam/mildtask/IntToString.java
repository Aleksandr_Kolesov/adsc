package com.epam.mildtask;

//    16.8 2 hours spent
public class IntToString
{
    private static String[] BEFORE_TWENTY = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
        "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
    private static String[] TENS = {"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
    private static String[] SEGMENT_NAMES = {"negative", "hundred", "", "thousand", "million", "billion"};

    public static String intToStringConverter(int x)
    {
        if (x == 0)
        {
            return BEFORE_TWENTY[0];
        }
        else if (x < 0)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(SEGMENT_NAMES[0]);
            stringBuilder.append(" ");
            stringBuilder.append(intToStringConverter(-1 * x));
            return stringBuilder.toString();
        }

        StringBuilder stringBuilder = new StringBuilder();
        int segmentIndex = 2;
        while (x != 0)
        {
            int segments = x % 1000;
            if (segments != 0)
            {
                stringBuilder.insert(0, " ");
                stringBuilder.insert(0, SEGMENT_NAMES[segmentIndex]);
                stringBuilder.insert(0, " ");
                stringBuilder.insert(0, segmentToString(segments));
            }
            x /= 1000;
            segmentIndex++;
        }

        return stringBuilder.toString().trim();
    }

    private static String segmentToString(int x)
    {
        StringBuilder stringBuilder = new StringBuilder();

        if (x >= 100)
        {
            int hundreds = x / 100;
            stringBuilder.append(BEFORE_TWENTY[hundreds]);
            stringBuilder.append(" ");
            stringBuilder.append(SEGMENT_NAMES[1]);
            x %= 100;
        }
        if (x >= 1 && x <= 19)
        {
            stringBuilder.append(" ");
            stringBuilder.append(BEFORE_TWENTY[x]);
        }
        else if (x >= 20)
        {
            stringBuilder.append(" ");
            stringBuilder.append(TENS[x / 10]);
            stringBuilder.append(" ");
            stringBuilder.append(BEFORE_TWENTY[x % 10]);
            x %= 10;
        }

        return stringBuilder.toString();
    }

}
