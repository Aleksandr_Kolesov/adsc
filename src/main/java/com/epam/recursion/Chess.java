package com.epam.recursion;

//8.12 1 hour spent
public class Chess
{
    public static boolean[][] getQueeпsPlaces(int gridSize)
    {
        boolean[][] grid = new boolean[gridSize][gridSize];
        getQueeпsPlaces(grid, 0);
        return grid;
    }

    private static void getQueeпsPlaces(boolean[][] grid, int row)
    {
        if (row != grid.length)
        {
            for (int column = 0; column < grid[row].length; column++)
            {

                if (isRightLocation(grid, column, row))
                {
                    grid[column][row] = true;
                    getQueeпsPlaces(grid, row + 1);
                }
            }
        }
        else
        {
            return;
        }
    }

    private static boolean isRightLocation(boolean[][] grid, int column, int row)
    {
        for (int i = 0; i < grid.length; i++)
        {
            if (grid[i][row])
            {
                return false;
            }
        }
        for (int j = 0; j < grid[column].length; j++)
        {
            if (grid[column][j])
            {
                return false;
            }
        }
        return true;
    }

}
