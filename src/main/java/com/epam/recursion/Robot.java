package com.epam.recursion;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

// 8.2 2 hour spent
public class Robot
{
    boolean[][] grid;

    public Robot(int r, int c)
    {
        grid = new boolean[r][c];
    }

    public void addBlocker(int x, int y)
    {
        if (x >= grid.length || x < 0 || y > grid[x].length || y < 0 || (x == 0 && y == grid[x].length - 1) ||
            (x == grid.length - 1 && y == 0))
        {
            throw new IllegalArgumentException();
        }
        grid[x][y] = true;
    }

    public boolean existsPath()
    {
        return existsPath(0, grid.length - 1, new ArrayList<>());
    }

    private boolean existsPath(int x, int y, List<Pair<Integer, Integer>> route)
    {
        ImmutablePair<Integer, Integer> step = new ImmutablePair<>(x, y);
        route.add(step);
        if (x == grid.length - 1 && y == 0)
        {
            return true;
        }
        boolean isRouteExist = false;
        if (isCellAvailable(x + 1, y))
        {
            isRouteExist = existsPath(x + 1, y, route);

        }
        if (!isRouteExist && isCellAvailable(x, y - 1))
        {
            isRouteExist = existsPath(x, y - 1, route);
        }
        if (!isRouteExist)
        {
            route.remove(step);
        }
        return isRouteExist;
    }

    private boolean isCellAvailable(int x, int y)
    {
        return x < grid.length && y >= 0 && !grid[x][y];

    }

    public static void main(String[] args)
    {
        Robot aa = new Robot(3, 3);
        aa.addBlocker(2, 2);
        boolean bb = aa.existsPath();
        System.out.println();
    }

}
