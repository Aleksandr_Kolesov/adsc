package com.epam.recursion;

import java.util.ArrayList;
import java.util.List;

//8.9 180 minutes spent
public class Parenthesis
{

    public static final char OPEN_SYMBOL = '(';
    public static final char CLOSE_SYMBOL = ')';

    public static List<String> genCorrectParens(int quantity)
    {
        List<String> parenthesisCombinations = new ArrayList<>();
        genCorrectParens(parenthesisCombinations, 0, 0, quantity, new char[quantity * 2], 0);
        return parenthesisCombinations;
    }

    private static void genCorrectParens(List<String> parenthesisCombinations, int openParentCounter, int closeParentCounter,
        int quantity, char[] arrayChars, int i)
    {
        if (openParentCounter != quantity || closeParentCounter != quantity)
        {
            if (openParentCounter < quantity)
            {
                arrayChars[i] = OPEN_SYMBOL;
                genCorrectParens(parenthesisCombinations, openParentCounter + 1, closeParentCounter, quantity,
                    arrayChars, i + 1);
            }
            if (openParentCounter > closeParentCounter)
            {
                arrayChars[i] = CLOSE_SYMBOL;
                genCorrectParens(parenthesisCombinations, openParentCounter, closeParentCounter + 1, quantity,
                    arrayChars, i + 1);
            }
        }
        else
        {
            parenthesisCombinations.add(new String(arrayChars));
        }
    }

}
