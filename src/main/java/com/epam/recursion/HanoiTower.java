package com.epam.recursion;

import java.util.Deque;
import java.util.LinkedList;

// 8.6 2 hours spent
public class HanoiTower
{
    public void moveAllDisks(int n, Tower tower1, Tower tower2, Tower tower3)
    {
        tower1.moveSeveralDisks(n, tower2, tower3);
    }

    static class Tower
    {
        private Deque<Integer> disksSizes;

        public Tower()
        {
            disksSizes = new LinkedList<>();
        }

        public Tower(int n)
        {
            this();
            for (int i = n - 1; i >= 0; i--)
            {
                this.addDisk(i);
            }

        }

        public void addDisk(int size)
        {
            if (disksSizes.isEmpty() || disksSizes.peek() > size)
            {
                disksSizes.push(size);
            }
        }

        public void moveSeveralDisks(int n, Tower tempTower, Tower destTower)
        {
            if (n > 0)
            {
                moveSeveralDisks(n - 1, destTower, tempTower);
                moveUpperDisk(destTower);
                tempTower.moveSeveralDisks(n - 1, this, destTower);
            }
        }

        public void moveUpperDisk(Tower destTower)
        {
            int upperDiskSize = disksSizes.pop();
            destTower.addDisk(upperDiskSize);
        }

        public int size()
        {
            return disksSizes.size();
        }
    }

}


















