package com.epam.recursion;

import java.util.ArrayList;
import java.util.List;

//8.4 2 hours spent
public class SubSet

{
    public static List<List<String>> generateSubSets(List<String> set, List<List<String>> subSets, int i)
    {
        if (i != set.size())
        {
            subSets = generateSubSets(set, subSets, i + 1);
            String member = set.get(i);
            List<List<String>> newSubSets = new ArrayList<>();
            for (List<String> subset : subSets)
            {
                List<String> newSubSet = new ArrayList<>();
                newSubSets.add(newSubSet);
                newSubSet.addAll(subset);
                newSubSet.add(member);
            }
            subSets.addAll(newSubSets);
        }
        else
        {
            subSets.add(new ArrayList<>());
        }
        return subSets;
    }
}
