package com.epam.recursion;

import java.util.HashSet;
import java.util.Set;

//8.7 1 hour spent
public class Permissions

{
    public static Set<String> generatePermissions(String string)
    {
        Set<String> permissions = new HashSet<>();
        generate(0, string.length(), permissions, string);
        return permissions;
    }

    private static void generate(int l, int r, Set<String> gen, String string)
    {
        char tmp;
        char[] array = string.toCharArray();
        if (l == r)
        {
            StringBuilder list = new StringBuilder();
            for (int ind = 0; ind < array.length; ind++)
            {
                list.append(array[ind]);
            }
            gen.add(list.toString());

        }
        else
        {
            for (int ind = l; ind < r; ind++)
            {
                tmp = array[l];
                array[l] = array[ind];
                array[ind] = tmp;
                generate(l + 1, r, gen, new String(array));
                tmp = array[l];
                array[l] = array[ind];
                array[ind] = tmp;
            }
        }
    }

    public static void main(String[] args)
    {
        Set<String> aa = new Permissions().generatePermissions("123");
        System.out.println();
    }
}
