package com.epam.recursion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//8.13 2 hours spent
public class Pile
{
    static class Box
    {
        int w, h, d;

        Box(int w, int h, int d)
        {
            this.w = w;
            this.h = h;
            this.d = d;
        }

        public boolean canBeLower(Box box)
        {
            return this.w > box.w && this.h > box.h && this.d > box.d;
        }
    }

    public static int createPile(List<Box> boxes)
    {
        List<Box> copyBoxes = new ArrayList<>(boxes);
        Collections.sort(copyBoxes, (o1, o2) -> o2.w - o1.w);
        int max = copyBoxes.get(0).h;
        for (int i = 0; i < copyBoxes.size(); i++)
        {
            max = Math.max(max, createPile(copyBoxes, i));
        }
        return max;
    }

    private static int createPile(List<Box> boxes, int i)
    {
        Box lastBox = boxes.get(i);
        int height = 0;
        for (int j = i + 1; j < boxes.size(); j++)
        {
            if (lastBox.canBeLower(boxes.get(j)))
            {
                height = Math.max(height, createPile(boxes, j));
            }
        }
        height += lastBox.h;
        return height;
    }

}
