package com.epam.mildtask;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SwapperTest
{
    @Test
    public void swap() throws Exception
    {
        List list = Arrays.asList(4, 9);
        List expected = Arrays.asList(9, 4);
        Swapper.swap(list, 0, 1);

        assertArrayEquals(expected.toArray(), list.toArray());
    }

}