package com.epam.mildtask;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class MaxSumSequenceTest
{
    @Test
    public void findMaxSumSequence() throws Exception
    {

        List<Integer> list = Arrays.asList(2, -8, 3, -2, 4, -10);
        Integer[] expected = {3, -2, 4};
        Pair<Integer, List<Integer>> pair = MaxSumSequence.findMaxSumSequence(list);

        int max = pair.getLeft().intValue();

        List<Integer> sequence = pair.getRight();

        assertEquals(5, max);
        assertArrayEquals(expected, sequence.toArray());
    }

}