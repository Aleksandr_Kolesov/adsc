package com.epam.mildtask;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class T9PredictorTest
{
    @Test
    public void predict() throws Exception
    {
        List<String> words = Arrays.asList("tree", "used", "hello", "open", "closed");
        String[] expected = {"tree", "used"};
        List<String> result = T9Predictor.predict("8733", words);
        assertArrayEquals(expected, result.toArray());
    }

}