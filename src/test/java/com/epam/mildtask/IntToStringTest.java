package com.epam.mildtask;

import org.junit.Test;

import static org.junit.Assert.*;

public class IntToStringTest
{
    @Test
    public void intToStringConverter() throws Exception
    {
        assertEquals("negative one hundred million  fifty six thousand seven hundred eighty nine",
            IntToString.intToStringConverter(-100056789));
    }

}