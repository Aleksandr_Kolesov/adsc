package com.epam.mildtask;

import org.junit.Test;

import static org.junit.Assert.*;

public class MathTest
{
    @Test
    public void delete() throws Exception
    {
        assertEquals(5, Math.delete(10, 5));
        assertEquals(15, Math.delete(10, -5));
        assertEquals(-5, Math.delete(-10, -5));
        assertEquals(-5, Math.delete(5, 10));
        assertEquals(-15, Math.delete(-10, 5));

    }

    @Test
    public void multiply() throws Exception
    {
        assertEquals(15, Math.multiply(3, 5));
        assertEquals(-15, Math.multiply(-3, 5));
        assertEquals(-15, Math.multiply(3, -5));

        assertEquals(15, Math.multiply(-3, -5));
        assertEquals(0, Math.multiply(0, -5));
        assertEquals(0, Math.multiply(5, 0));
    }

    @Test
    public void divide() throws Exception
    {
        assertEquals(2, Math.divide(10, 5));
        assertEquals(0, Math.divide(5, 10));
        assertEquals(2, Math.divide(14, 5));
        assertEquals(-2, Math.divide(-10, 5));
        assertEquals(-2, Math.divide(10, -5));
        assertEquals(2, Math.divide(-10, -5));
    }

}