package com.epam.mildtask;

import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class XMLEncoderTest
{

    public static final String EXPECTED_STRING = "1 4 McDowell 5 CA 0 2 3 Gayle 0 some message 0 0";

    @Test
    public void encodeXML() throws Exception
    {
        Map<String, Integer> values = new HashMap<String, Integer>()
        {{
            put("family", 1);
            put("person", 2);
            put("firstName", 3);
            put("lastName", 4);
            put("state", 5);
        }};

        assertEquals(EXPECTED_STRING, XMLEncoder.encodeXML("src/main/resources/test.xml", values));
    }

}