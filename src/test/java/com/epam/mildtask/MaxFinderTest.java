package com.epam.mildtask;

import org.junit.Test;

import static org.junit.Assert.*;

public class MaxFinderTest
{
    @Test
    public void max() throws Exception
    {
            int max = MaxFinder.max(4, 9);
            assertEquals(9,max);
    }

}