package com.epam.mildtask;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;


public class LakeSizeCalculatorTest
{
    @Test
    public void calculateLakesSizes() throws Exception
    {
        int[][] land = {
            {0, 2, 1, 0},
            {0, 1, 0, 1},
            {1, 1, 0, 1},
            {0, 1, 0, 1}
        };
        List<Integer> result = LakeSizeCalculator.calculateLakesSizes(land);
        assertEquals(3, result.size());
        assertTrue(result.containsAll(Arrays.asList(2, 4, 1)));
    }

}