package com.epam.mildtask;

import org.junit.Test;

import static org.junit.Assert.*;

public class LineMaxPointsFinderTest
{
    @Test
    public void findMaxPointsInterceptor() throws Exception
    {
        LineMaxPointsFinder.Point[] points = {
            new LineMaxPointsFinder.Point(10, 10),
            new LineMaxPointsFinder.Point(20, 20),
            new LineMaxPointsFinder.Point(60, 60),
            new LineMaxPointsFinder.Point(30, 30),
            new LineMaxPointsFinder.Point(40, 40),
            new LineMaxPointsFinder.Point(0, 10),
            new LineMaxPointsFinder.Point(200, 50),
            new LineMaxPointsFinder.Point(100, 25)
        };
        LineMaxPointsFinder.Line result = LineMaxPointsFinder.findMaxPointsInterceptor(points);
        assertEquals(0.0, result.interception.doubleValue(), 0);
        assertEquals(1.0, result.slope.doubleValue(), 0);
    }

}