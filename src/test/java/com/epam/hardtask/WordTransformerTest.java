package com.epam.hardtask;

import org.junit.Test;

import java.util.Deque;

import static org.junit.Assert.*;

public class WordTransformerTest
{
    @Test
    public void transformWord() throws Exception
    {
        String[] words = {"DAMP","LIKE","LAMP","LIMP","LIME","LIKE"};
        String[] expected = {"DAMP","LAMP","LIMP","LIME","LIKE"};
        Deque<String> result = WordTransformer.transformWord("DAMP", "LIKE", words);
        assertArrayEquals(expected,result.toArray());
    }

}