package com.epam.hardtask;

import org.junit.Test;

import static org.junit.Assert.*;

public class HistogramWaterVolumeTest
{
    @Test
    public void calculateHistogramWaterVolume() throws Exception
    {
        int[]array = {0, 0, 4, 0, 0, 6, 0, 0, 3, 0, 5, 0, 1, 0, 0, 0};
        int result = HistogramWaterVolume.calculateHistogramWaterVolume(array);
        assertEquals(26,result);
    }

}