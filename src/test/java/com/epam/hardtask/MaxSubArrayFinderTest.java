package com.epam.hardtask;

import org.junit.Test;

import static org.junit.Assert.*;

public class MaxSubArrayFinderTest
{
    @Test
    public void find() throws Exception
    {
        char[] array = {'1', 'C', '1', 'C', '1', 'C', '1', 'C', '1', 'C', '1', 'C', 'C', 'C'};
        char[] result = MaxSubArrayFinder.find(array);
        char[] expected = {'1', 'C', '1', 'C', '1', 'C', '1', 'C', '1', 'C', '1', 'C'};
        assertArrayEquals(expected, result);
    }

}