package com.epam.hardtask;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class NamesCounterTest
{
    @Test
    public void mergeSynonymousNames() throws Exception
    {
        Map<String, Integer> nameCounter = new HashMap<String, Integer>()
        {{
            put("John", 15);
            put("Jon", 12);
            put("Chris", 13);
            put("Kris", 4);
            put("Christopher", 19);
        }};
        Map<String, List<String>> synonyms = new HashMap<String, List<String>>()
        {{
            put("Jon", Arrays.asList("John"));
            put("John", Arrays.asList("Johnny"));
            put("Chris", Arrays.asList("Kris", "Christopher"));
        }};

        Map<String, Integer> result = NamesCounter.mergeSynonymousNames(nameCounter, synonyms);
        assertEquals(27, result.get("Jon").intValue());
        assertEquals(36, result.get("Chris").intValue());
    }

}