package com.epam.hardtask;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class MinimalsNumbersFinderTest
{
    @Test
    public void findMinimalNumbers() throws Exception
    {
        List<Integer> list = Arrays.asList(1, 5, 7, 5, 9, 4, 3, 10, 23, 58, 78, 42, 42, 42, 36, 44, 45, 43);
        List<Integer> result = MinimalsNumbersFinder.findMinimalNumbers(list, 5);
        Integer[] expected = {1, 3, 4, 5, 5};
        assertArrayEquals(expected,result.toArray());


    }

}