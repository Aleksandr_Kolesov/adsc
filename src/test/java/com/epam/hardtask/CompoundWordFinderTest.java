package com.epam.hardtask;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class CompoundWordFinderTest
{
    @Test
    public void findCompoundWord() throws Exception
    {
        List<String> words = Arrays.asList("ba", "cat", "banana", "dog", "nana", "walker", "dogwalker");
        String result = CompoundWordFinder.findCompoundWord(words);
        assertEquals("dogwalker", result);
    }

}