package com.epam.sorter;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class PeaksSorterTest
{
    @Test
    public void sortPeaks() throws Exception
    {
        List<Integer> list = Arrays.asList(7, 8, 5, 2, 3);
        List<Integer> expected = Arrays.asList(2, 5, 3, 8, 7);
        PeaksSorter.sortPeaks(list);
        assertArrayEquals(expected.toArray(), list.toArray());

    }

}