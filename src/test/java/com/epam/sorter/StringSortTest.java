package com.epam.sorter;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;

public class StringSortTest
{
    List<String> INPUT_DATA = Arrays.asList("CAB", "BAC", "DFG", "cba", "ABCD", "CDBA", "DDDDDD", "EEEE", "GFC", "fgtyfc", "ABC");
    List<String> EXPECTED = Arrays.asList("CAB", "BAC", "cba", "ABC", "ABCD", "CDBA", "fgtyfc", "GFC", "DDDDDD", "DFG", "EEEE");

    @Test
    public void sort() throws Exception
    {

        List<String> result = new StringSort().sort(INPUT_DATA);
        assertArrayEquals(EXPECTED.toArray(), result.toArray());

    }

}