package com.epam.recursion;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class SubSetTest
{
    private static final String[][] EXPECTED_SUBSETS = {{}, {"3"}, {"2"}, {"3", "2"}, {"1"}, {"3", "1"}, {"2", "1"},
        {"3", "2", "1"}};

    @Test
    public void generateSubSets() throws Exception
    {
        List<String> list = Arrays.asList("1", "2", "3");
        List<List<String>> subsets = SubSet.generateSubSets(list, new ArrayList<>(), 0);
        assertArrayEquals(EXPECTED_SUBSETS, toArray(subsets));
    }

    private String[][] toArray(List<List<String>> list)
    {

        String[][] array = new String[list.size()][];
        for (int i = 0; i < array.length; i++)
        {
            if (list.get(i).size() == 0)
            {
                array[i] = new String[0];
            }
            array[i] = new String[list.get(i).size()];
            array[i] = list.get(i).toArray(array[i]);
        }
        return array;
    }

}