package com.epam.recursion;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HanoiTowerTest
{
    @Test
    public void moveDisks() throws Exception
    {
        HanoiTower hanoiTower = new HanoiTower();
        int n = 5;
        HanoiTower.Tower tower1 = new HanoiTower.Tower(n);
        HanoiTower.Tower tower2 = new HanoiTower.Tower();
        HanoiTower.Tower tower3 = new HanoiTower.Tower();
        assertEquals(n, tower1.size());
        assertEquals(0, tower2.size());
        assertEquals(0, tower3.size());
        hanoiTower.moveAllDisks(n, tower1, tower2, tower3);
        assertEquals(0, tower1.size());
        assertEquals(0, tower2.size());
        assertEquals(n, tower3.size());

    }

}