package com.epam.recursion;

import org.junit.Test;

import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.*;

public class PermissionsTest
{
    @Test
    public void generatePermissions() throws Exception
    {
        Set<String> result = Permissions.generatePermissions("123");
        assertEquals(6, result.size());
        assertTrue(result.containsAll(Arrays.asList("132", "231", "123", "321", "213", "312")));
    }

}