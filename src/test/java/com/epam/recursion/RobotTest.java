package com.epam.recursion;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RobotTest
{
    @Test
    public void findRoute() throws Exception
    {
        Robot robot = new Robot(4, 4);
        robot.addBlocker(1, 0);
        robot.addBlocker(1, 1);
        robot.addBlocker(1, 2);
        assertTrue(robot.existsPath());
        robot.addBlocker(1, 3);
        assertFalse(robot.existsPath());
    }

}