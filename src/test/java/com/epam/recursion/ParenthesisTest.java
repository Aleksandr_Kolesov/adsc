package com.epam.recursion;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ParenthesisTest
{
    @Test
    public void genCorrectParens() throws Exception
    {
        List<String> result = Parenthesis.genCorrectParens(3);
        assertEquals(5, result.size());
        assertTrue(result.containsAll(Arrays.asList("()()()", "(()())", "((()))", "()(())", "(())()")));

    }

}