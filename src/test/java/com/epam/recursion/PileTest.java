package com.epam.recursion;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PileTest
{
    @Test
    public void createPile() throws Exception
    {
        List<Pile.Box> boxes = new ArrayList<>();

        boxes.add(new Pile.Box(1, 2, 3));
        boxes.add(new Pile.Box(4, 5, 6));
        boxes.add(new Pile.Box(7, 8, 9));
        boxes.add(new Pile.Box(10, 11, 12));
        boxes.add(new Pile.Box(13, 14, 15));

        assertEquals(40, Pile.createPile(boxes));

        boxes.add(new Pile.Box(13, 13, 13));

        assertEquals(40, Pile.createPile(boxes));

    }

}