package com.epam.recursion;

import org.junit.Test;

import static org.junit.Assert.*;

public class ChessTest
{
    @Test
    public void getQueeпsPlaces() throws Exception
    {
        boolean[][] result = Chess.getQueeпsPlaces(8);
        assertTrue(result[0][0]);
        assertTrue(result[1][1]);
        assertTrue(result[2][2]);
        assertTrue(result[3][3]);
        assertTrue(result[4][4]);
        assertTrue(result[5][5]);
        assertTrue(result[6][6]);
        assertTrue(result[7][7]);

    }

}