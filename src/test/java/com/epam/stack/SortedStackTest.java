package com.epam.stack;

import org.junit.Test;

import static org.junit.Assert.*;

public class SortedStackTest
{
    @Test
    public void push() throws Exception
    {
        SortedStack<Integer> instance = new SortedStack<>();
        instance.push(20);
        instance.push(30);
        instance.push(10);
        instance.push(40);
        instance.push(15);
        instance.push(27);
        assertEquals("10->15->20->27->30->40", instance.toString());
    }

}