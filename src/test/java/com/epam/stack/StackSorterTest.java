package com.epam.stack;

import org.junit.Test;

import static org.junit.Assert.*;

public class StackSorterTest
{
    @Test
    public void sort() throws Exception
    {
        MinStack<Integer> instance = new MinStack<>();
        instance.push(20);
        instance.push(30);
        instance.push(10);
        instance.push(40);
        instance.push(15);
        instance.push(27);
        MinStack<Integer> sortedStack = StackSorter.sort(instance);
        assertEquals("10->15->20->27->30->40", sortedStack.toString());
    }

}