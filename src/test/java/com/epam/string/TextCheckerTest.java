package com.epam.string;

import org.junit.Test;

import static org.junit.Assert.*;

public class TextCheckerTest
{
    private TextChecker instance = new TextChecker();

    @Test
    public void isReshuffle() throws Exception
    {
        assertTrue(instance.isReshuffle("ABCDEF","FEDCBA"));
        assertFalse(instance.isReshuffle("ABCDEF","FEDCBAA"));
    }

    @Test
    public void isTextWithUniqSymbolsTest() throws Exception
    {
        assertTrue(instance.isTextWithUniqSymbols("uniq Symbols"));
        assertFalse(instance.isTextWithUniqSymbols("don't uniq Symbols"));

    }

    @Test
    public void isPalindromePermutionTest() throws Exception
    {
        assertFalse(instance.isPalindromePermution("don't palindrome permution"));
        assertTrue(instance.isPalindromePermution("Tact   Coa"));

    }

    @Test
    public void isDistanceOfOneModificationTest() throws Exception
    {
        assertTrue(instance.isDistanceOfOneModification("pale", "ple"));
        assertTrue(instance.isDistanceOfOneModification("pales", "pale"));
        assertTrue(instance.isDistanceOfOneModification("pale", "bale"));
        assertFalse(instance.isDistanceOfOneModification("pale", "bake"));

    }

    @Test
    public void compressStringTest() throws Exception
    {
        assertEquals("a2b1c5a3", instance.compressString("aabcccccaaa"));
        assertEquals("abcdefg", instance.compressString("abcdefg"));
    }

}