package com.epam.graph;

import org.junit.Test;

import java.util.Deque;

import static org.junit.Assert.*;

public class GraphTest
{
    @Test
    public void findOrderProjectsTest() throws Exception
    {
        Graph graph = new Graph(6);
        graph.addEdge(3, 0);
        graph.addEdge(1, 5);
        graph.addEdge(3, 1);
        graph.addEdge(0, 5);
        graph.addEdge(2, 3);
        assertEquals(6, graph.findOrderProjects().size());

        Graph graph2 = new Graph(6);
        graph2.addEdge(3, 0);
        graph2.addEdge(1, 5);
        graph2.addEdge(3, 1);
        graph2.addEdge(0, 5);
        graph2.addEdge(2, 3);
        graph2.addEdge(5, 0);
        assertNull(graph2.findOrderProjects());

    }

    @Test
    public void haveRoute() throws Exception
    {
        Graph graph = new Graph(10);
        graph.addEdge(1, 5);
        graph.addEdge(5, 7);
        graph.addEdge(7, 9);
        assertTrue(graph.haveRoute(1, 9));
        assertTrue(graph.haveRoute(1, 5));
        assertTrue(graph.haveRoute(5, 9));

        assertFalse(graph.haveRoute(9, 1));
        assertFalse(graph.haveRoute(4, 9));

    }

}