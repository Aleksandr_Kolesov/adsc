package com.epam.graph;

import com.epam.linkedlist.MyLinkedList;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class TreeTest
{
    @Test
    public void isSubTree() throws Exception
    {
        Random random = new Random();
        int rootNameIndex = 10 + random.nextInt(90);
        Tree tree1 = new Tree();
        Tree tree2 = new Tree();
        for (int i = 0; i < 100; i++)
        {
            Tree.Node root = tree1.addRandomSheet(i);
            if (i == rootNameIndex)
            {
                tree2.root = root;
            }

        }
        Tree tree3 = new Tree();
        for (int i = 101; i < 150; i++)
        {
            tree3.addRandomSheet(i);
        }
        assertFalse(tree3.isSubTree(tree1));
        assertTrue(tree2.isSubTree(tree1));
        assertTrue(tree1.isSubTree(tree1));
    }

    @Test
    public void createLinkedLists() throws Exception
    {
        Tree tree = new Tree();
        tree.addRandomSheet(1);
        tree.addRandomSheet(2);
        tree.addRandomSheet(4);
        tree.addRandomSheet(5);
        tree.addRandomSheet(6);
        tree.addRandomSheet(7);
        tree.addRandomSheet(8);
        tree.addRandomSheet(9);
        MyLinkedList[] linkedLists = tree.createLinkedLists();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < linkedLists.length; i++)
        {
            if (linkedLists[i] != null)
            {
                stringBuilder.append(linkedLists[i].toString());
                stringBuilder.append("\n");
            }
        }
        String result = stringBuilder.toString();
        assertTrue(result.contains("1"));
        assertTrue(result.contains("2"));
        assertTrue(result.contains("4"));
        assertTrue(result.contains("5"));
        assertTrue(result.contains("6"));
        assertTrue(result.contains("7"));
        assertTrue(result.contains("8"));
        assertTrue(result.contains("9"));

    }

}