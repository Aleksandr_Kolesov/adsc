package com.epam.linkedlist;

import org.junit.Test;

import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

import static org.junit.Assert.*;

public class MyLinkedListTest
{
    @Test
    public void addLast() throws Exception
    {
        MyLinkedList<Integer> instance = new MyLinkedList<>();
        assertEquals(0, instance.getSize());
        instance.addLast(11);
        assertEquals(1, instance.getSize());
        assertEquals(11, instance.getFirst().getData().intValue());
        assertEquals(11, instance.getLast().getData().intValue());
        instance.addLast(22);
        assertEquals(2, instance.getSize());
        assertEquals(11, instance.getFirst().getData().intValue());
        assertEquals(22, instance.getLast().getData().intValue());
        instance.addLast(33);
        assertEquals(3, instance.getSize());
        assertEquals(11, instance.getFirst().getData().intValue());
        assertEquals(22, instance.getFirst().getNext().getData().intValue());
        assertEquals(33, instance.getLast().getData().intValue());
    }

    @Test
    public void delete() throws Exception
    {

        MyLinkedList<Integer> instance = new MyLinkedList<>();
        instance.addLast(11);
        instance.addLast(22);
        instance.addLast(33);
        instance.addLast(44);
        instance.addLast(55);
        instance.delete(11);
        assertEquals(4, instance.getSize());
        assertEquals(22, instance.getFirst().getData().intValue());
        assertEquals(33, instance.getFirst().getNext().getData().intValue());
        assertEquals(44, instance.getLast().getPrev().getData().intValue());
        assertEquals(55, instance.getLast().getData().intValue());
        instance.delete(55);
        assertEquals(3, instance.getSize());
        assertEquals(22, instance.getFirst().getData().intValue());
        assertEquals(33, instance.getFirst().getNext().getData().intValue());
        assertEquals(44, instance.getLast().getData().intValue());
        instance.delete(33);
        assertEquals(2, instance.getSize());
        assertEquals(22, instance.getFirst().getData().intValue());
        assertEquals(44, instance.getLast().getData().intValue());
        instance.delete(22);
        instance.delete(44);
        assertEquals(0, instance.getSize());
        assertNull(instance.getFirst());
        assertNull(instance.getLast());

    }

    @Test
    public void isPalindrome() throws Exception
    {
        MyLinkedList<String> instance = new MyLinkedList<>();
        instance.addLast("a");
        instance.addLast("b");
        instance.addLast("c");
        instance.addLast("b");
        instance.addLast("a");
        assertTrue(MyLinkedList.isPalindrome(instance));

        MyLinkedList<String> instance2 = new MyLinkedList<>();
        instance2.addLast("a");
        instance2.addLast("b");
        instance2.addLast("b");
        instance2.addLast("a");
        assertTrue(MyLinkedList.isPalindrome(instance2));

        MyLinkedList<String> instance3 = new MyLinkedList<>();
        instance3.addLast("a");
        assertTrue(MyLinkedList.isPalindrome(instance3));

        MyLinkedList<String> instance4 = new MyLinkedList<>();
        instance4.addLast("a");
        instance4.addLast("b");
        assertFalse(MyLinkedList.isPalindrome(instance4));
    }

    @Test
    public void dropDublicates() throws Exception
    {
        MyLinkedList<Integer> instance = new MyLinkedList<>();
        Set<Integer> set = new LinkedHashSet<>();
        Random random = new Random();
        for (int i = 0; i < 10; i++)
        {
            int data = random.nextInt(5);
            instance.addLast(data);
            set.add(data);
        }
        MyLinkedList.dropDuplicates(instance);
        assertEquals(setToString(set), instance.toString());

    }

    @Test
    public void splitList() throws Exception
    {
        MyLinkedList<Integer> instance = new MyLinkedList<>();
        instance.addLast(3);
        instance.addLast(5);
        instance.addLast(8);
        instance.addLast(5);
        instance.addLast(10);
        instance.addLast(2);
        instance.addLast(1);
        MyLinkedList<Integer> splittedlList = MyLinkedList.splitList(instance, 5);
        System.out.println();
    }

    @Test
    public void sum() throws Exception
    {
        MyLinkedList<Integer> x = new MyLinkedList<>();
        MyLinkedList<Integer> y = new MyLinkedList<>();
        x.addLast(7);
        x.addLast(1);
        x.addLast(6);
        y.addLast(5);
        y.addLast(9);
        y.addLast(2);
        MyLinkedList<Integer> directOrder = MyLinkedList.sum(x, y, true);
        assertEquals("2->1->9", directOrder.toString());

        MyLinkedList<Integer> z = new MyLinkedList<>();
        MyLinkedList<Integer> w = new MyLinkedList<>();
        z.addLast(6);
        z.addLast(1);
        z.addLast(7);
        w.addLast(2);
        w.addLast(9);
        w.addLast(5);
        MyLinkedList<Integer> reverseOrder = MyLinkedList.sum(z, w, false);
        assertEquals("9->1->2", reverseOrder.toString());
    }

    private String setToString(Set<Integer> set)
    {
        StringBuilder stringBuilder = new StringBuilder();
        set.forEach(element -> {
            stringBuilder.append(element);
            stringBuilder.append("->");
        });
        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());

        return stringBuilder.toString();
    }

}