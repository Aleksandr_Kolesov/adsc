package com.epam.testtasks;

import org.junit.Test;

import java.util.Arrays;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class GraphWithRemoverTest
{
    @Test
    public void removeVertexWith2Edge() throws Exception
    {
        GraphWithRemover graph = new GraphWithRemover(4);
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(2, 3);
        graph.addEdge(0, 3);
        assertEquals(4, countNotNullElemnts(graph.nodes));
        graph.removeVertexWith2Edge();
        assertEquals(2, countNotNullElemnts(graph.nodes));
        assertEquals(1, countNotNullElemnts(graph.nodes[1].children));
        assertEquals(1, countNotNullElemnts(graph.nodes[2].children));
        assertNotNull(graph.nodes[1]);
        assertNotNull(graph.nodes[2]);
        assertTrue(graph.nodes[1].children[0] == graph.nodes[2]);
        assertTrue(graph.nodes[2].children[0] == graph.nodes[1]);
    }

    private int countNotNullElemnts(Object[] array)
    {
        return (int) Arrays.stream(array).filter(Objects::nonNull).count();
    }

}