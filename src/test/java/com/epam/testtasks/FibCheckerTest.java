package com.epam.testtasks;

import org.junit.Test;

import static org.junit.Assert.*;

public class FibCheckerTest
{
    @Test
    public void check() throws Exception
    {
        {
            assertTrue(FibChecker.check("13213455"));
            assertTrue(FibChecker.check("1321345589"));
            assertTrue(FibChecker.check("011"));
            assertFalse(FibChecker.check("13"));
            assertFalse(FibChecker.check("133213455"));
        }
    }

}